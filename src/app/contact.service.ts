import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { User } from './user';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http:HttpClient) { }
  //url="https://fir-b7f69.firebaseio.com/product.json"
  url="https://reqres.in/api/products/3"
  add(data:User){
   return this.http.post(this.url,data)
  }

}
