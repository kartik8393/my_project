import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MycompComponent } from './mycomp/mycomp.component';
import { MacompComponent } from './macomp/macomp.component';
import { HomeComponent } from './home/home.component';
import { TeamComponent } from './team/team.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {
    path:'mycomp',
    component: MycompComponent
  },
  {
    path:'macomp',
    component: MacompComponent
  },
  {
    path:'home',
    component: HomeComponent
  },
  {
    path:'team',
    component: TeamComponent
  },
  {
    path:'contact',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents= [MycompComponent, MacompComponent, HomeComponent, TeamComponent, ContactComponent]