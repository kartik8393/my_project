import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MacompComponent } from './macomp.component';

describe('MacompComponent', () => {
  let component: MacompComponent;
  let fixture: ComponentFixture<MacompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MacompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
