import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import {HttpClient} from '@angular/common/http'
import {ContactService}  from '../contact.service'
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
 
  constructor(private service:ContactService ) { }
  technology=['Web Development', 'Cloud','Mobile Application']
  ngOnInit() {
  }

  
  
  onSubmit(mydata:any){
    console.log(mydata.value)
    this.service.add(mydata)
    .subscribe(
      data=>console.log(data)
    )
  }
}
