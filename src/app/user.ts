export class User {
    constructor(
        public name: string,
        public number: number,
        public email: string,
        public technology: string

    ){}
}
